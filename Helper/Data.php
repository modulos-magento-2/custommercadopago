<?php

namespace Eparts\CustomMercadoPago\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\ScopeInterface;

class Data extends AbstractHelper
{
    const SYSTEM_MESSAGE_DETAILS = 'payment/mercadopago_custom/message_details';

    /**
     * Get message error details
     *
     * @return bool
     */
    public function getMessageErrorDetails()
    {
        return $this->scopeConfig->getValue(self::SYSTEM_MESSAGE_DETAILS, ScopeInterface::SCOPE_STORE);
    }
}
