<?php

namespace Eparts\CustomMercadoPago\Plugin\Model;

use Eparts\CustomMercadoPago\Helper\Data;
use Magento\Framework\Serialize\Serializer\Json;
use Psr\Log\LoggerInterface;

class Core
{
    /**
     * @var Data
     */
    protected $helper;

    /**
     * @var Json
     */
    protected $json;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * Constructor.
     *
     * @param Data $helper
     * @param Json $json
     * @param LoggerInterface $logger
     */
    public function __construct(
        Data $helper,
        Json $json,
        LoggerInterface $logger
    )
    {
        $this->helper = $helper;
        $this->json = $json;
        $this->logger = $logger;
    }

    /**
     * around getMessageError \MercadoPago\Core\Model\Core
     *
     * @param \MercadoPago\Core\Model\Core $subject
     * @param \Closure $proceed
     * @param $response
     * @return string
     */
    public function aroundGetMessageError(
        \MercadoPago\Core\Model\Core $subject,
        \Closure $proceed,
        $response
    )
    {
        $messageErrorCode = 'NOT_IDENTIFIED';

        if (isset($response['response']) &&
            isset($response['response']['cause']) &&
            count($response['response']['cause']) > 0
        ) {
            // get first error
            $cause = $response['response']['cause'][0];
            if (isset($cause['code'])) {
                //if exist get message error
                $messageErrorCode = $cause['code'];
            }
        }
        $this->logger->info('bbbbbbbbbbbb: ' . $messageErrorCode);

        $messageDetails = $this->json->unserialize($this->helper->getMessageErrorDetails());
        foreach ($messageDetails as $item => $_message) {
            if ($messageErrorCode == $_message['code']) {
                return $_message['message'];
            }
        }

        return $proceed($response);
    }
}
