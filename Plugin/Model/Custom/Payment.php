<?php

namespace Eparts\CustomMercadoPago\Plugin\Model\Custom;

use Eparts\CustomMercadoPago\Helper\Data;
use Magento\Framework\Serialize\Serializer\Json;
use Psr\Log\LoggerInterface;

class Payment
{
    /**
     * @var Data
     */
    protected $helper;

    /**
     * @var Json
     */
    protected $json;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * Constructor.
     *
     * @param Data $helper
     * @param Json $json
     * @param LoggerInterface $logger
     */
    public function __construct(
        Data $helper,
        Json $json,
        LoggerInterface $logger
    )
    {
        $this->helper = $helper;
        $this->json = $json;
        $this->logger = $logger;
    }

    /**
     * around getRejectedStatusDetailMessage \MercadoPago\Core\Model\Custom\Payment
     *
     * @param \MercadoPago\Core\Model\Custom\Payment $subject
     * @param \Closure $proceed
     * @param string $status_detail
     * @return string
     */
    public function aroundGetRejectedStatusDetailMessage(
        \MercadoPago\Core\Model\Custom\Payment $subject,
        \Closure $proceed,
        $status_detail
    )
    {
        $this->logger->info('aaaaaaaaaaaa: ' . $status_detail);

        $messageDetails = $this->json->unserialize($this->helper->getMessageErrorDetails());
        foreach ($messageDetails as $item => $_message) {
            $this->logger->info('aaaaaaaaaaaa: ' . $status_detail . ' - ' . $_message['code']);
            if ($status_detail == $_message['code']) {
                $this->logger->info('aaaaaaaaaaaa: ' . $_message['message']);
                return __($_message['message']);
            }
        }

        return $proceed($status_detail);
    }
}
